# Automatic Games Server - Counter Strike: Global Offensive server

[Counter Strike: Global Offensive](https://blog.counter-strike.net/)

This repository contains the required code to be able to spin up a game server, in a DevOps way.

At the end you will got an ec2 instance in your configured AWS account with an **installed Counter Strike: Global Offensive server**, in automatic way. This code was used to create a virtual team-building.

## Table of contents

- [Automatic Games Server - Counter Strike: Global Offensive server](#automatic-games-server---counter-strike-global-offensive-server)
  - [Table of contents](#table-of-contents)
  - [First usage](#first-usage)
    - [HowTo - Create and configure SSH-key](#howto---create-and-configure-ssh-key)
    - [Terraform binary](#terraform-binary)
    - [Configured GSLT code for server](#configured-gslt-code-for-server)
  - [Used technologies & Software](#used-technologies--software)
    - [Terraform](#terraform)
    - [Ansible](#ansible)
    - [Visual Studio Code](#visual-studio-code)
  - [vagrant folder (Optional - for local development)](#vagrant-folder-optional---for-local-development)
    - [Purpose](#purpose)
    - [Usage](#usage)
  - [Describe files](#describe-files)
    - [connection.tf](#connectiontf)
    - [network.tf](#networktf)
    - [outputs.tf](#outputstf)
    - [servers.tf](#serverstf)
    - [variables.tf](#variablestf)
  - [Custom Counter Strike: Global Offensive settings](#custom-counter-strike-global-offensive-settings)
  - [How-To play your in your new server](#how-to-play-your-in-your-new-server)
  - [Common issues](#common-issues)
  - [Terraform documentation](#terraform-documentation)
  - [Requirements](#requirements)
  - [Providers](#providers)
  - [Modules](#modules)
  - [Resources](#resources)
  - [Inputs](#inputs)
  - [Outputs](#outputs)
  - [License](#license)

## First usage

**[AWS account](https://aws.amazon.com/free/) (has free tier - credit card required) is a requirement**, as this terraform code will create resources on it. Please pay attention for **input section under `Terraform documentation` at the end of this readme**.

### HowTo - Create and configure SSH-key

Check the following [markdown documentation](Documentation/ssh_key/ssh_key.md). Please keep in mind, that SSH-key will generated on-the-fly by terraform, you do not need to save it / use it (it will be used by configuration management tool - aka ansible for post-configuration). If you would like to **connect and login** to the created instance, you can do it in an easier way as well: **<https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/session-manager.html>**

### Terraform binary

Before you start to use this code, **terraform must to be installed**. Please follow official guides about [how to install it](https://learn.hashicorp.com/terraform/getting-started/install.html), or if you are familiar with choco, simply install it like `choco install terraform`. If it was done, please apply **"terraform init"** command (in this repository's folder) to initialize required providers.

Also important to **provide required environment variables** for terraform to be able to connect to your personal AWS account. Here is an example, **how to do it under windows (terminal in VSCode)**:

``` bash
$env:AWS_SECRET_ACCESS_KEY  = 'lbahbalhbalhblah'
$env:AWS_ACCESS_KEY_ID  = 'AKIABLAHBALBHALHALHAL'
$env:AWS_DEFAULT_REGION  = 'eu-west-1'
$env:AWS_REGION  = 'eu-west-1'
```

- Important!: You should execute terraform on the same terminal sessions where you exported AWS variables as environment variable.

As a result you will got a plan what you can apply with **"terraform apply"** command. When you finished the game, you can destroy all created resource with **"terraform destroy"** command.

### Configured GSLT code for server

To be able to build a CS:GO server provided by steam, you need a GSLT code. For more information, please visit [this page](https://docs.linuxgsm.com/steamcmd/gslt).

After it **please add you personalized GSLT code to [csgoserver.cfg](vagrant/provision/csgoserver.cfg).**

## Used technologies & Software

### Terraform

Terraform **creates infrastructure** under applications. In this example AWS provider is in use.

### Ansible

Ansible **installs automatically applications / adjust OS** settings.

### Visual Studio Code

This **IDE** was used to prepare and **handle all of the code**.

## vagrant folder (Optional - for local development)

[Vagrant](https://www.vagrantup.com/) is a hashicorp tool which provides you a way to **spin up a virtual machine in your local computer (via virtualbox)**. It is ideal to reproduce the server in your local computer, without any cloud provider.

### Purpose

Try / Check / Adjust and fine-tune the game server without any additional price.

### Usage

Change your directory to the vagrant folder (in IDE / terminal) and execute `vagrant up` command which will create a virtual machine based on configured "Vagrantfile".
After that you can connect to your dedicated Counter Strike: Global Offensive server in `192.168.56.56` address, port 27015 (default).

## Describe files

### connection.tf

Store provider(s) like AWS. This is a terraform plugin which "provide" classes.

### network.tf

Adjust network settings, required to create any instance within in. Most of the parameters are variables to use them during instance creation (servers.tf)

### outputs.tf

Define output values, displayed at the end of terraform's running.

### servers.tf

Describe instances & bootstrap code.

### variables.tf

Definition of variables, created / used in code for terraform.

## Custom Counter Strike: Global Offensive settings

All fine-tune and modifications are in:

- [csgoserver.cfg](vagrant/provision/csgoserver.cfg)
- [server_config](/vagrant/provision/server_config)
- [gamemode_competitive.cfg](/vagrant/provision/gamemode_competitive.cfg)

## How-To play your in your new server

You have multiple way, how to connect to your customer server. Here is an [example](https://nodecraft.com/support/games/csgo/how-to-quickly-find-and-join-your-cs-go-server).

## Common issues

Check the following [markdown documentation](Documentation/common_issues/common_issues.md).

<!-- BEGIN_TF_DOCS -->
## Terraform documentation

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12.21 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.38 |
| <a name="requirement_local"></a> [local](#requirement\_local) | >= 2.1.0 |
| <a name="requirement_null"></a> [null](#requirement\_null) | >= 3.1.0 |
| <a name="requirement_tls"></a> [tls](#requirement\_tls) | >= 3.1.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.59.0 |
| <a name="provider_local"></a> [local](#provider\_local) | 2.1.0 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.1.0 |
| <a name="provider_tls"></a> [tls](#provider\_tls) | 3.1.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_ec2_tag.spot_tags_automation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_tag) | resource |
| [aws_ec2_tag.spot_tags_cost_category](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_tag) | resource |
| [aws_ec2_tag.spot_tags_name](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_tag) | resource |
| [aws_iam_instance_profile.ssm_profile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_role.iam_for_ssm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.ssm-tags-role-policy-attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_internet_gateway.internet-gateway-terraform](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway) | resource |
| [aws_key_pair.generated_key](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/key_pair) | resource |
| [aws_route.internet_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_security_group.csgo](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group.ssh](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_spot_instance_request.csgo_spot](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/spot_instance_request) | resource |
| [aws_subnet.private_subnet_b](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.public_subnet_a](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_vpc.automation-network](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc) | resource |
| [local_file.ssh_key](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file) | resource |
| [null_resource.csgo_post_install](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [tls_private_key.ssh_key](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/resources/private_key) | resource |
| [aws_ami.Amazon_Linux](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ami_user"></a> [ami\_user](#input\_ami\_user) | SSH user, used for login to linux instance. Depends on used AMI. | `string` | `"ec2-user"` | no |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | AWS instance type. Define size of machine. **Spot price depends on instance type!**. <https://aws.amazon.com/ec2/instance-types/> | `string` | `"t3a.medium"` | no |
| <a name="input_managed_policies"></a> [managed\_policies](#input\_managed\_policies) | n/a | `list` | <pre>[<br>  "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore",<br>  "arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess"<br>]</pre> | no |
| <a name="input_private_key"></a> [private\_key](#input\_private\_key) | Path of your private SSH key (where to generate on-the-fly). Required to connect target instance via SSH. | `string` | `"./secrets/csgo.key"` | no |
| <a name="input_region"></a> [region](#input\_region) | AWS region. Where to deploy with this Infrastructure-As-A-Code - terraform. | `string` | `"us-east-1"` | no |
| <a name="input_spot_price"></a> [spot\_price](#input\_spot\_price) | Your maximum price for the spot instance. **Spot price depends on instance type!**. Check it in <https://aws.amazon.com/ec2/spot/pricing/> | `number` | `"0.013"` | no |
| <a name="input_ssh_key_name"></a> [ssh\_key\_name](#input\_ssh\_key\_name) | SSH key's name, used for linux instances. The key itself will be generated by terraform on-the-fly. | `string` | `"csgo"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_Counter_Strike_Global_Offensive_server_address"></a> [Counter\_Strike\_Global\_Offensive\_server\_address](#output\_Counter\_Strike\_Global\_Offensive\_server\_address) | IP address of created cs go server. |

<!-- END_TF_DOCS -->

## License

MIT

---
Copyright © 2021, Peter Mikaczo
