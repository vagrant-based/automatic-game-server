provider "aws" {
  region = var.region
  default_tags {
    tags = {
      Name          = "counter_strike_go"
      Automation    = "true"
      cost_category = "csgo"
    }
  }
}
