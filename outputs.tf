output "Counter_Strike_Global_Offensive_server_address" {
  description = "IP address of created cs go server."
  value       = aws_spot_instance_request.csgo_spot.public_ip
}