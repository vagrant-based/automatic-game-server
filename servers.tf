# Request a spot instance at $0.03
resource "aws_spot_instance_request" "csgo_spot" {
  ami                            = data.aws_ami.Amazon_Linux.id
  spot_price                     = var.spot_price
  instance_type                  = var.instance_type
  key_name                       = aws_key_pair.generated_key.key_name
  subnet_id                      = aws_subnet.public_subnet_a.id
  wait_for_fulfillment           = "true"
  spot_type                      = "one-time"
  instance_interruption_behavior = "terminate"
  iam_instance_profile           = aws_iam_instance_profile.ssm_profile.name


  root_block_device {
    volume_type           = "gp2"
    volume_size           = "40"
    delete_on_termination = "true"
  }

  vpc_security_group_ids = [
    aws_security_group.ssh.id,
    aws_security_group.csgo.id,
    aws_security_group.csgo_rcon.id,
  ]

}


#spot tags workaround

resource "aws_ec2_tag" "spot_tags_name" {
  resource_id = aws_spot_instance_request.csgo_spot.spot_instance_id
  key         = "Name"
  value       = "csgo"
}

resource "aws_ec2_tag" "spot_tags_automation" {
  resource_id = aws_spot_instance_request.csgo_spot.spot_instance_id
  key         = "Automation"
  value       = "true"
}

resource "aws_ec2_tag" "spot_tags_cost_category" {
  resource_id = aws_spot_instance_request.csgo_spot.spot_instance_id
  key         = "cost_category"
  value       = "csgo"
}


# SSM

resource "aws_iam_role" "iam_for_ssm" {
  name               = "IAM-ssm"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

variable "managed_policies" {
  default = ["arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore",
    "arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess",
  ]
}

resource "aws_iam_role_policy_attachment" "ssm-tags-role-policy-attach" {
  count      = length(var.managed_policies)
  policy_arn = element(var.managed_policies, count.index)
  role       = aws_iam_role.iam_for_ssm.name
}

resource "aws_iam_instance_profile" "ssm_profile" {
  name = "ssm_profile"
  role = aws_iam_role.iam_for_ssm.name
}

#SSH_KEY --> for ec2 instance
resource "tls_private_key" "ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = var.ssh_key_name
  public_key = tls_private_key.ssh_key.public_key_openssh
}

resource "local_file" "ssh_key" {
  content  = tls_private_key.ssh_key.private_key_pem
  filename = var.private_key
}

resource "null_resource" "csgo_post_install" {

  provisioner "remote-exec" {
    inline = [
      "sudo mkdir /provision",
      "sudo chmod 777 /provision",
    ]

    connection {
      host        = aws_spot_instance_request.csgo_spot.public_ip
      type        = "ssh"
      user        = var.ami_user
      private_key = file(var.private_key)
    }
  }

  provisioner "file" {
    source      = "vagrant/provision/gsm.yaml"
    destination = "/tmp/gsm.yaml"

    connection {
      host        = aws_spot_instance_request.csgo_spot.public_ip
      type        = "ssh"
      user        = var.ami_user
      private_key = file(var.private_key)
    }
  }

  provisioner "file" {
    source      = "vagrant/provision/os_dependencies.sh"
    destination = "/tmp/os_dependencies.sh"

    connection {
      host        = aws_spot_instance_request.csgo_spot.public_ip
      type        = "ssh"
      user        = var.ami_user
      private_key = file(var.private_key)
    }
  }

  provisioner "file" {
    source      = "vagrant/provision/csgoserver"
    destination = "/provision/csgoserver"

    connection {
      host        = aws_spot_instance_request.csgo_spot.public_ip
      type        = "ssh"
      user        = var.ami_user
      private_key = file(var.private_key)
    }
  }

  provisioner "file" {
    source      = "vagrant/provision/csgoserver.cfg"
    destination = "/provision/csgoserver.cfg"

    connection {
      host        = aws_spot_instance_request.csgo_spot.public_ip
      type        = "ssh"
      user        = var.ami_user
      private_key = file(var.private_key)
    }
  }

  provisioner "file" {
    source      = "vagrant/provision/server_config"
    destination = "/provision/server_config"

    connection {
      host        = aws_spot_instance_request.csgo_spot.public_ip
      type        = "ssh"
      user        = var.ami_user
      private_key = file(var.private_key)
    }
  }

  provisioner "file" {
    source      = "vagrant/provision/gamemode_competitive.cfg"
    destination = "/provision/gamemode_competitive.cfg"

    connection {
      host        = aws_spot_instance_request.csgo_spot.public_ip
      type        = "ssh"
      user        = var.ami_user
      private_key = file(var.private_key)
    }
  }

  provisioner "file" {
    source      = "vagrant/provision/csgo.service"
    destination = "/provision/csgo.service"

    connection {
      host        = aws_spot_instance_request.csgo_spot.public_ip
      type        = "ssh"
      user        = var.ami_user
      private_key = file(var.private_key)
    }
  }

  provisioner "file" {
    source      = "vagrant/provision/send_webhook.sh"
    destination = "/tmp/send_webhook.sh"

    connection {
      host        = aws_spot_instance_request.csgo_spot.public_ip
      type        = "ssh"
      user        = var.ami_user
      private_key = file(var.private_key)
    }
  }

  provisioner "remote-exec" {
    inline = [
      "sudo chmod +x /tmp/os_dependencies.sh",
      "sudo /tmp/os_dependencies.sh",
      "ansible-playbook /tmp/gsm.yaml",
      "sudo chmod +x /tmp/send_webhook.sh",
      "sudo /tmp/send_webhook.sh"
    ]

    connection {
      host        = aws_spot_instance_request.csgo_spot.public_ip
      type        = "ssh"
      user        = var.ami_user
      private_key = file(var.private_key)
    }
  }
}